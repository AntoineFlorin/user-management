<?php require "_head.html.php"; ?>
<?php require "_nav.html.php"; ?>
        <main role="main" class="container">
            <h1>DWWM - Session</h1>
            <h2>Welcome</h2>
<?php if($this->isConnected): ?>        
            <p>Welcome <?= $this->user->login; ?> to this home page.</p>
<?php else: ?>
            <p>
                To connect please use following accounts : (same login and password)
                <ul>
                    <li>user</li>
                    <li>admin</li>
                    <li>account</li>
                    <li>tech</li>
                    <li>secr</li>
                    <li>dir</li>
                    <li>techdir</li>
                </ul>
            </p>
<?php endif; ?>
        </main>
<?php require "_body-end.html.php"; ?>